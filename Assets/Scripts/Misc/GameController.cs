﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public GameObject selectionCirclePrefab;

    public GameObject planetNormalPrefab;
    public GameObject planetMercenaryPrefab;
    public GameObject planetGuardianPrefab;
    public GameObject planetRegeneratorPrefab;
    public GameObject planetNuclearSiloPrefab;
    public GameObject planetDisruptorPrefab;

    public GameObject planetNuclearSiloMisslePrefab;

    public GameObject _laserPrefab;

    [SerializeField]
    private Canvas _winCanvas;
    [SerializeField]
    private Canvas _loseCanvas;
    [SerializeField]
    private Canvas _pauseCanvas;

    public static GameStates GameState = GameStates.Playing;

    public CinematicController CinematicController;
    public DialogueManager DialogueManager;
    public TransformMotion CinematicPanel;

    public List<Faction> Factions = new List<Faction>();

    private void Awake()
    {
        Utility.GameController = this;
        SelectionCircle = Instantiate(selectionCirclePrefab, Vector3.one * 9999f, Quaternion.identity);
        StageControl.Init(Factions);
    }

    private void Start()
    {
        Utility.InputChecker();

        // Start the game with no selection
        Utility.DeactivateSelectionCircle();

        // Play the music from the start of the first stage
        if (!Utility.musicIsPlaying)
        {
            Utility.musicIsPlaying = true;
            Utility.musicManager.PlayMusic(Utility.musicManager.music, 0.375f, true);
        }

        Invoke("ActivateCinematic", 0.2f);
    }

    private void ActivateCinematic()
    {
        CinematicController.Progress();
    }

    // Update is called once per frame
    private void Update()
    {
        Utility.InputChecker();

        // Check for the win and lose condition every 60 frames
        if (Time.frameCount % 60 == 0)
        {
            CheckStateConditions();
        }
    }

    public GameObject SelectionCircle { get; set; }

    /// <summary>
    /// Checks the state conditions, win and lose.
    /// </summary>
    private void CheckStateConditions()
    {
        if (Utility.PlayerHasWon(Utility.playerTeam) && GameState == GameStates.Playing)
        {
            GameState = GameStates.Win;
            Utility.musicManager.PlaySound(Utility.musicManager.musicSource, Utility.musicManager.win, 0.8f);
            _winCanvas.enabled = true;
        }
        else if (Utility.LoseCondition() && GameState == GameStates.Playing)
        {
            GameState = GameStates.Lose;
            Utility.musicManager.PlaySound(Utility.musicManager.musicSource, Utility.musicManager.lose, 0.8f);
            _loseCanvas.enabled = true;
        }
    }
}
