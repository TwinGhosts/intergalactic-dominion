﻿using UnityEngine.UI;
using UnityEngine;

public class DisplayRating : MonoBehaviour
{
    private void Awake()
    {
        GetComponent<Text>().text += "<color='yellow'>" + CurrentPlayer.data.onlineRating + "</color>";
    }
}
