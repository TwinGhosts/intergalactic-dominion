﻿using MEC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test1_6_AI : MonoBehaviour
{
    [SerializeField]
    private List<Planet> _initialAttackPlanets = new List<Planet>();
    [SerializeField]
    private List<Planet> _planetsToAttack = new List<Planet>();

    [SerializeField]
    private float _timeUntillAttack = 15f;

    private bool _runOnce = true;

    private void Update()
    {
        if (GameController.GameState == GameStates.Playing && _runOnce)
        {
            _runOnce = false;
            Timing.RunCoroutine(AttackRoutine());
        }
    }

    private IEnumerator<float> AttackRoutine()
    {
        yield return Timing.WaitForSeconds(_timeUntillAttack);
        Attack();

        yield return Timing.WaitForSeconds(_timeUntillAttack);
        Timing.RunCoroutine(AttackRoutine());
    }

    private void Attack()
    {
        foreach (var planet in _initialAttackPlanets)
        {
            if (planet.team == Team.Team2)
            {
                planet.AttackTarget(_planetsToAttack[(int)Random.Range(0, _planetsToAttack.Count - 1)]);
            }
        }
    }
}
