﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Campaign1 : ActionControllerBase
{
    /// <summary>
    /// Campaign Mission 01: The Start of a Rebellion
    /// </summary>
    protected override void InitCinematicActions()
    {
        _cinematicActions.Add(new CinematicActionScreenShake(
            2f,
            2f,
            true));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.SPECTER,
            DialogueDirection.Right,
            "VINDICARS!!!"));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.SPECTER,
            DialogueDirection.Right,
            "DO NOT LET HIM ESCAPE!"));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.ZED,
            DialogueDirection.Left,
            "Damn it! No matter what I try, I can't shake them off..."));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.ZED,
            DialogueDirection.Left,
            "Damn it! No matter what I try, I can't shake them off..."));
    }
}
