﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

/// <summary>
/// The cinematic base class
/// </summary>
public abstract class CinematicActionBase
{
    public bool WaitUntillFinished = true;
    public abstract void Activate();
}

/// <summary>
/// A dialogue action which activates the dialogue box and feeds it lines of dialogue
/// </summary>
public class CinematicActionDialogue : CinematicActionBase
{
    private string _dialogue;
    private Character _character;
    private DialogueManager _dialogueManager;
    private DialogueDirection _dialogueDirection;

    public CinematicActionDialogue(DialogueManager dialogueManager, Character character, DialogueDirection dialogueDirection, string dialogue)
    {
        _dialogue = dialogue;
        _character = character;
        _dialogueManager = dialogueManager;
        _dialogueDirection = dialogueDirection;
    }

    public override void Activate()
    {
        Timing.RunCoroutine(_dialogueManager.SendDialogue(_dialogueDirection, _character, _dialogue), Segment.Update, "CINEMATIC_TEXT");
    }
}

/// <summary>
/// A cinematic camera action which lets the camera move from one point to another
/// </summary>
public class CinematicActionCameraMotion : CinematicActionBase
{
    private float _duration;
    private Vector3 _endPosition;

    public CinematicActionCameraMotion(Vector3 endPosition, float duration, bool waitUntillEnd)
    {
        _duration = duration;
        WaitUntillFinished = waitUntillEnd;
        _endPosition = endPosition;
    }

    public override void Activate()
    {
        Timing.RunCoroutine(CameraMovement.MoveCinematicCameraOverTime(_endPosition, _duration, null, WaitUntillFinished), Segment.Update, "CINEMATIC_MOTION");
    }
}

/// <summary>
/// A cinematic camera action which lets the camera move from one point to another
/// </summary>
public class CinematicActionCameraFollow : CinematicActionBase
{
    private float _duration;
    private Transform _target;

    public CinematicActionCameraFollow(Transform target, float duration, bool waitUntillEnd)
    {
        _duration = duration;
        WaitUntillFinished = waitUntillEnd;
        _target = target;
    }

    public override void Activate()
    {
        Timing.RunCoroutine(CameraMovement.FollowCinematicTargetForDuration(_target, _duration, WaitUntillFinished), Segment.Update, "CINEMATIC_FOLLOW");
    }
}

/// <summary>
/// A cinematic action which lets the screen shake for a duration when called
/// </summary>
public class CinematicActionScreenShake : CinematicActionBase
{
    private float _duration;
    private float _intensity;

    public CinematicActionScreenShake(float duration, float intensity, bool waitUntillEnd)
    {
        _duration = duration;
        WaitUntillFinished = waitUntillEnd;
        _intensity = intensity;
    }

    public override void Activate()
    {
        Timing.RunCoroutine(CameraMovement.ShakeCinematicCamera(_intensity, _duration, WaitUntillFinished), Segment.Update, "CINEMATIC_SHAKE");
    }
}
