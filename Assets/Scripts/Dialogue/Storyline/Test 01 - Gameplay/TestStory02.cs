﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestStory02 : ActionControllerBase
{
    /// <summary>
    /// Campaign Mission 01: The Start of a Rebellion
    /// </summary>
    protected override void InitCinematicActions()
    {
        _cinematicActions.Add(new CinematicActionScreenShake(1f, 0f, true));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "Now for your next challenge...      \nDefeat a basic AI opponent using the same battlefield as the previous stage, with some modifications ofcourse.\nWe dont wan't you to learn too quickly!"));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "Neutral planets are found within this scenario.\nNeutrals don't produce ships and have no active special effects, there are some exceptions though, but you will see that for yourself soon enough."));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "Use the Neutral planets as an outpost and use them to build up a sizeable force to beat your enemy."));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "Oh and remember: the bigger the planet, the higher its ship creation cap, the faster it generates ships and the better its passive effects!"));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "Good luck!"));
    }
}
