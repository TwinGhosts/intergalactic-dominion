﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestStory03 : ActionControllerBase
{
    /// <summary>
    /// Campaign Mission 01: The Start of a Rebellion
    /// </summary>
    protected override void InitCinematicActions()
    {
        _cinematicActions.Add(new CinematicActionScreenShake(1f, 0f, true));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "For this stage you will need to take over the <color='cyan'>Guardian</color> planet, the one with the shield."));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "The <color='cyan'>Guardian</color> planet has shield which absorbs attacks. After a limited amount of attacks, based on the size of the planet are absorbed it will be vulnerable for a short time before it regains its shield!"));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "The <color='cyan'>Guardian</color> planet also generates ship with stronger attack power, three to be precise!"));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "Good luck and strike swift!"));
    }
}
