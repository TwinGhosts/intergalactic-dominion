﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestStory01 : ActionControllerBase
{
    /// <summary>
    /// Campaign Mission 01: The Start of a Rebellion
    /// </summary>
    protected override void InitCinematicActions()
    {
        _cinematicActions.Add(new CinematicActionScreenShake(1f, 0f, true));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "Welcome to the <color='cyan'>Horizon</color>: <color='yellow'>Vindication</color> Gameplay Test 1! \n\nPress ENTER to continue..."));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "In this test you will learn to control your planets and defeat a basic enemy with different planets and ships!"));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "The first stage is without an opponent. Try to conquer the red planets on your own."));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "Good luck!"));
    }
}
