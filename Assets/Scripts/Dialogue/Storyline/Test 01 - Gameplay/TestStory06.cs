﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestStory06 : ActionControllerBase
{
    /// <summary>
    /// Campaign Mission 01: The Start of a Rebellion
    /// </summary>
    protected override void InitCinematicActions()
    {
        _cinematicActions.Add(new CinematicActionScreenShake(1f, 0f, true));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "In this scenario you will face an opponent with a fleet of strong ships."));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "To counter this we have given you control over a <color='blue'>Disruptor</color> planet. \nThis planet slows all hostile ships around it for a fair amount, but it does not produce ships. The effective range is based on the size of the planet."));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "Survive the attack and retalliate!"));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "Good luck!"));
    }
}
