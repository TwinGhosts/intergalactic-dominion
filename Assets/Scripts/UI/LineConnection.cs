﻿using UnityEngine;

public class LineConnection : MonoBehaviour
{
    [SerializeField]
    private GameObject[] _connections;

    // Use this for initialization
    private void Start()
    {
        for (var i = 0; i < _connections.Length; i++)
        {
            Utility.DrawLine(transform.position, _connections[i].transform.position, Color.white);
        }
    }

    // Update is called once per frame
    private void Update()
    {

    }
}