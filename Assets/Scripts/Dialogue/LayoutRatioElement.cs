﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
[ExecuteInEditMode]
public class LayoutRatioElement : UIBehaviour, ILayoutElement
{
    [SerializeField]
    private float _widthWeight = 1;
    [SerializeField]
    private float _heightWeight = 1;
    [SerializeField]
    private RectTransform.Axis _axis;

    [SerializeField]
    private DrivenRectTransformTracker _tracker;

    private RectTransform _rectTransform;

    public RectTransform.Axis Axis
    {
        get
        {
            return _axis;
        }
        set
        {
            _axis = value;
        }
    }

    public float minWidth
    {
        get
        {
            if (_axis == RectTransform.Axis.Horizontal)
            {
                return GetComponent<RectTransform>().rect.height * (_widthWeight / _heightWeight);
            }
            else
            {
                return -1;
            }
        }
    }

    public float preferredWidth
    {
        get
        {
            return -1;
        }
    }

    public float flexibleWidth
    {
        get
        {
            if (_axis == RectTransform.Axis.Vertical)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }
    }

    public float minHeight
    {
        get
        {
            if (_axis == RectTransform.Axis.Vertical)
            {
                return GetComponent<RectTransform>().rect.width * (_heightWeight / _widthWeight);
            }
            else
            {
                return -1;
            }
        }
    }

    public float preferredHeight
    {
        get
        {
            return -1;
        }
    }

    public float flexibleHeight
    {
        get
        {
            if (_axis == RectTransform.Axis.Horizontal)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }
    }

    public int layoutPriority
    {
        get
        {
            return 1;
        }
    }

    private void Update()
    {
        SetDirty();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        SetDirty();
    }

    protected override void OnTransformParentChanged()
    {
        SetDirty();
    }

    /// <summary>
    ///   <para>See MonoBehaviour.OnDisable.</para>
    /// </summary>
    protected override void OnDisable()
    {
        SetDirty();
        base.OnDisable();
    }

    protected override void OnDidApplyAnimationProperties()
    {
        SetDirty();
    }

    protected override void OnBeforeTransformParentChanged()
    {
        SetDirty();
    }

#if UNITY_EDITOR
    protected override void OnValidate()
    {
        SetDirty();
    }
#endif

    protected void SetDirty()
    {
        if (!IsActive())
        {
            return;
        }

        LayoutRebuilder.MarkLayoutForRebuild(transform as RectTransform);
    }

    public void CalculateLayoutInputHorizontal()
    {
    }

    public void CalculateLayoutInputVertical()
    {
    }
}
