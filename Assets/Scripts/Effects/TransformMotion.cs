﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

public class TransformMotion : MonoBehaviour
{
    [SerializeField]
    private Vector3 _endPosition;
    [SerializeField]
    private Vector3 _startPosition;

    [SerializeField]
    private AnimationCurve _motionCurve = AnimationCurve.Constant(0f, 1f, 1f);

    [SerializeField]
    private float _motionSpeed = 1f;

    [SerializeField]
    private bool _is2D = true;

    public void Open()
    {
        Timing.RunCoroutine(OpenMotion());
    }

    public void Close()
    {
        Timing.RunCoroutine(CloseMotion());
    }

    /// <summary>
    /// Moves the transform from the startPosition to the endPosition
    /// </summary>
    /// <returns></returns>
    private IEnumerator<float> OpenMotion()
    {
        var progress = 0f;
        while (progress < 1f)
        {
            if (_is2D)
            {
                GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(_startPosition, _endPosition, _motionCurve.Evaluate(progress));
            }
            else
            {
                transform.position = Vector3.Lerp(_startPosition, _endPosition, _motionCurve.Evaluate(progress));
            }
            progress += Time.deltaTime * _motionSpeed;
            yield return Timing.WaitForOneFrame;
        }
    }

    /// <summary>
    /// Closes the transform from the endPosition to the startPosition
    /// </summary>
    /// <returns></returns>
    private IEnumerator<float> CloseMotion()
    {
        var progress = 0f;
        while (progress < 1f)
        {
            if (_is2D)
            {
                GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(_endPosition, _startPosition, _motionCurve.Evaluate(progress));
            }
            else
            {
                transform.position = Vector3.Lerp(_endPosition, _startPosition, _motionCurve.Evaluate(progress));
            }
            progress += Time.deltaTime * _motionSpeed;
            yield return Timing.WaitForOneFrame;
        }
    }
}
