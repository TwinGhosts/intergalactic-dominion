﻿using UnityEngine;

public class AICommander : Commander
{
    //[SerializeField]
    private AIStratagems _stratagem = AIStratagems.RANDOM;

    public AIStratagems Stratagem
    {
        get
        {
            return _stratagem;
        }
    }
}

public enum AIStratagems
{
    BALANCED,
    AGRESSIVE,
    DEFENSIVE,
    SPECIALITY,
    RANDOM,
    CUSTOM,
}
