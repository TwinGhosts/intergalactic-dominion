﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using MEC;

public abstract class Planet : MonoBehaviour
{
    // Variables
    [Header("Planet Game Properties")]
    [SerializeField]
    private string _planetName = "";

    public PlanetType type = PlanetType.Normal;
    public PlanetSize size = PlanetSize.Normal;
    public Team team = Team.Neutral;

    private bool _isAddingPower = false;

    public int initialPower = 0;
    public float powerGain = 1f;

    [SerializeField]
    private float _attackRate = 1f;

    [SerializeField]
    private float _attackDamage = 1f;

    [SerializeField]
    private float _lazerSpeed = 7f;

    protected Dictionary<PlanetConnectionKey, PlanetConnectionInfo> _connectionList = new Dictionary<PlanetConnectionKey, PlanetConnectionInfo>(new PlanetConnectionKey.EqualityComparer());

    protected SpriteRenderer _teamGlowRing;
    protected SpriteRenderer _outerRing;

    protected Text _powerCounterText;

    [SerializeField]
    private bool _generatePower = true;

    protected float _currentPower;
    protected float _maxPower;
    protected float MAX_POWER;
    protected float _chargeRate;
    protected float CHARGE_RATE;

    protected float _rotationSpeed = 15f;

    // Methods
    public virtual void AttackTarget(Planet attackingPlanet, Planet planetToAttack)
    {
        var connectionInfo = new PlanetConnectionInfo(attackingPlanet, planetToAttack, _lazerSpeed);
        var connection = new PlanetConnectionKey(attackingPlanet, planetToAttack);

        if (_connectionList.ContainsKey(connection))
        {
            Debug.Log("ERROR > Connection already exists!");
        }
        else if (planetToAttack.ConnectionInfo.ContainsKey(new PlanetConnectionKey(planetToAttack, attackingPlanet)))
        {
            if (attackingPlanet.team == planetToAttack.team)
            {
                Debug.Log("ERROR > Can't double connect friendly planets");
            }
            else
            {
                Debug.Log("SUCCES > Double connecting two different team planets");
            }
        }
        else
        {
            _connectionList.Add(connection, connectionInfo);
            StageControl.ConnectionInfo.Add(connection, connectionInfo);
        }
    }

    public void BreakAttackingConnections()
    {
        foreach (var planet in StageControl.AllPlanets)
        {
            planet.BreakConnection(planet, this);
        }
    }

    // Methods
    public virtual void AttackTarget(Planet attackingPlanet)
    {
        Debug.Log("Wrong Attacking Method");
    }

    public virtual void BreakConnection(Planet fromPlanet, Planet toPlanet)
    {
        PlanetConnectionInfo _connectionInfo;
        var _connection = new PlanetConnectionKey(fromPlanet, toPlanet);

        _connectionList.TryGetValue(_connection, out _connectionInfo);
        if (_connectionInfo != null && !_connectionInfo.delete)
        {
            _connectionInfo.BreakConnection();
            _connectionList.Remove(_connection);
        }
    }

    public virtual void Update()
    {
        _outerRing.transform.rotation = _teamGlowRing.transform.rotation =
            Quaternion.Euler(_outerRing.transform.rotation.eulerAngles + (Vector3.forward * _rotationSpeed * Time.deltaTime));

        foreach (var dictInfo in _connectionList)
        {
            var value = dictInfo.Value;
            value.UpdateConnectionProgress();
        }

        if (GameController.GameState == GameStates.Playing)
        {
            IsFullBehaviour();
            UpdatePowerCounter();
        }

        UpdateTeamColor();
    }

    public virtual void UpdatePowerCounter()
    {
        _powerCounterText.text = "" + _currentPower;
    }

    public virtual void UpdateTeamColor()
    {
        _teamGlowRing.color = Utility.GetTeamColor(team);
    }

    public virtual IEnumerator<float> AddPowerRoutine(float interval, float amount)
    {
        _isAddingPower = true;
        yield return Timing.WaitForSeconds(interval);
        _isAddingPower = false;
        _currentPower += amount;
    }

    public virtual void AddPower(float amount)
    {
        _currentPower += amount;
    }

    protected virtual void IsFullBehaviour()
    {
        // Text Color
        if (!IsFull)
        {
            if (team == Team.Neutral)
            {
                _powerCounterText.color = Color.gray;
            }
            else
            {
                _powerCounterText.color = Color.white;
            }
        }

        // Create ships when it isn't full
        if (!IsFull && team != Team.Neutral && _generatePower && !_isAddingPower)
        {
            Timing.RunCoroutine(AddPowerRoutine(_chargeRate, powerGain));
        }
        else if (IsFull)
        {
            _powerCounterText.color = Color.red;
        }
    }

    /// <summary>
    /// Remove a ship from the planet and update the counter
    /// </summary>
    /// <param name="ship"></param>
    public virtual void RemovePower(float amount)
    {
        _currentPower -= amount;
        if (_currentPower <= 0f)
        {
            _currentPower = 0f;
            BreakAllConnections();
        }
    }

    /// <summary>
    /// Remove a ship from the planet and update the counter
    /// </summary>
    /// <param name="ship"></param>
    public virtual void RemovePower(float amount, Planet attacker)
    {
        if (attacker && _currentPower - amount <= 0f)
        {
            _currentPower = 0f;
            StageControl.SwitchPlanetOwner(attacker, this);
        }
    }

    public void BreakAllConnections()
    {
        List<PlanetConnectionKey> _tempKeyList = new List<PlanetConnectionKey>();

        foreach (var connectionDictInfo in _connectionList.Keys)
        {
            var startingPlanet = connectionDictInfo.fromPlanet.transform.position;
            var endPlanet = connectionDictInfo.toPlanet.transform.position;
            _tempKeyList.Add(connectionDictInfo);
        }

        foreach (var key in _tempKeyList)
        {
            PlanetConnectionInfo _pInfo;
            StageControl.ConnectionInfo.TryGetValue(key, out _pInfo);
            _pInfo.startingPlanet.BreakConnection(key.fromPlanet, key.toPlanet);
        }
    }

    protected virtual void SetStats()
    {
        switch (size)
        {
            case PlanetSize.Tiny:
                MAX_POWER = 20;
                CHARGE_RATE = 1.5f;
                _attackRate = 1.0f;
                transform.localScale = Vector2.one * 0.8f;
                break;
            case PlanetSize.Small:
                MAX_POWER = 50;
                CHARGE_RATE = 1.25f;
                _attackRate = 0.85f;
                transform.localScale = Vector2.one * 1.05f;
                break;
            default:
            case PlanetSize.Normal:
                MAX_POWER = 75;
                CHARGE_RATE = 1.0f;
                _attackRate = 0.7f;
                transform.localScale = Vector2.one * 1.3f;
                break;
            case PlanetSize.Large:
                MAX_POWER = 100;
                CHARGE_RATE = 0.8f;
                _attackRate = 0.55f;
                transform.localScale = Vector2.one * 1.6f;
                break;
            case PlanetSize.Huge:
                MAX_POWER = 150;
                CHARGE_RATE = 0.4f;
                _attackRate = 0.4f;
                transform.localScale = Vector2.one * 1.95f;
                break;
            case PlanetSize.Gigantic:
                MAX_POWER = 250;
                CHARGE_RATE = 0.2f;
                _attackRate = 0.25f;
                transform.localScale = Vector2.one * 2.5f;
                break;
        }

        GetComponent<CircleCollider2D>().radius = 0.4f;
        _chargeRate = CHARGE_RATE;
        _maxPower = MAX_POWER;
    }

    protected virtual void Init()
    {
        // Set the basic stats of the planet
        Utility.planetsActive = true;
        SetStats();
    }

    public virtual Dictionary<PlanetConnectionKey, PlanetConnectionInfo> ConnectionInfo
    {
        get
        {
            return _connectionList;
        }
    }

    public virtual bool IsFull
    {
        get
        {
            return _currentPower >= _maxPower;
        }
    }

    public string PlanetName
    {
        get
        {
            return _planetName;
        }
    }

    public float Power
    {
        get
        {
            return _currentPower;
        }
    }

    public bool CanGeneratePower
    {
        get
        {
            return _generatePower;
        }
    }

    public float MaxPower
    {
        get
        {
            return MAX_POWER;
        }
    }

    public float AttackRate
    {
        get
        {
            return _attackRate;
        }
    }

    public float AttackPower
    {
        get
        {
            return _attackDamage;
        }
    }

    public override string ToString()
    {
        return "Type: " + type + " | Size: " + size + " | Team: " + team;
    }
}