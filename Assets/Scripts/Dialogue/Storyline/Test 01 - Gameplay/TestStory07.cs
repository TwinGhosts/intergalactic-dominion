﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestStory07 : ActionControllerBase
{
    /// <summary>
    /// Campaign Mission 01: The Start of a Rebellion
    /// </summary>
    protected override void InitCinematicActions()
    {
        _cinematicActions.Add(new CinematicActionScreenShake(1f, 0f, true));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "In this scenario you will train with a <color='magenta'>Nuclear Silo</color> planet."));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "This planet charges to full over a period of time when controlled by a Commander. When fully charged, it launches a rocket which attacks a random hostile planet for a percentage of their maximum capacity of ships."));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "The damage is based on the size of the planet and when the planet reduces the ship count of the planet to zero or lower it will become neutral."));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "A great counter against enemies who don't attack or enemies with large planets! \nGood luck!"));
    }
}
