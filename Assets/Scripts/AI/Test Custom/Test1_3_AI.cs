﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

public class Test1_3_AI : MonoBehaviour
{
    [SerializeField]
    private float _timeUntillSupport = 8f;
    [SerializeField]
    private float _timeUntillAttack = 15f;

    [SerializeField]
    private Planet _soloAttackPlanet;

    private bool _runOnce = true;

    private void Update()
    {
        if (GameController.GameState == GameStates.Playing && _runOnce)
        {
            _runOnce = false;
            Timing.RunCoroutine(SupportRoutine());
            Timing.RunCoroutine(AttackRoutine());
        }
    }

    private IEnumerator<float> AttackRoutine()
    {
        yield return Timing.WaitForSeconds(_timeUntillAttack);

        var playerPlanetList = StageControl.TeamPlanetList(Team.Team1);
        if (_soloAttackPlanet.team == Team.Team2)
        {
            _soloAttackPlanet.AttackTarget(playerPlanetList[(int)Random.Range(0f, playerPlanetList.Count - 1f)]);
        }
        else
        {
            var alliedPlanets = StageControl.TeamPlanetList(Team.Team2);
            foreach (var planet in alliedPlanets)
            {
                planet.AttackTarget(_soloAttackPlanet);
            }
        }

        yield return Timing.WaitForSeconds(_timeUntillAttack);
        Timing.RunCoroutine(AttackRoutine());
    }

    private IEnumerator<float> SupportRoutine()
    {
        yield return Timing.WaitForSeconds(_timeUntillSupport);
        Support();

        Timing.RunCoroutine(SupportRoutine());
    }

    private void Support()
    {
        if (_soloAttackPlanet.team == Team.Team2)
        {
            var alliedPlanets = StageControl.TeamPlanetList(Team.Team2);
            foreach (var planet in alliedPlanets)
            {
                if (_soloAttackPlanet != planet)
                {
                    planet.AttackTarget(alliedPlanets[(int)Random.Range(0, alliedPlanets.Count - 1)]);
                }
            }
        }
    }
}
