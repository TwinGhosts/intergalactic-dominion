﻿using UnityEngine;

public class PlanetDisruptor : PlanetBehaviour
{
    private SpriteRenderer _rotator, _rotatorGlow;
    private ParticleSystem _particleSystem;
    private ParticleSystem.MainModule _particleSystemModule;

    [SerializeField]
    private float range = 4f;

    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();
        var sprites = GetComponentsInChildren<SpriteRenderer>();
        _rotator = sprites[2];
        _rotatorGlow = sprites[3];
        _particleSystem = GetComponentInChildren<ParticleSystem>();
        _particleSystemModule = _particleSystem.main;
        _particleSystemModule.startSize = range;

        var color = Utility.GetTeamColor(team);
        color.a = 0.15f;
        _particleSystemModule.startColor = color;
        _particleSystem.Play();
        SetStats();
    }

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        _rotationSpeed *= 1.5f;
    }

    public override void Update()
    {
        if (team != Team.Neutral)
        {
            base.Update();

            _rotator.transform.rotation = _rotatorGlow.transform.rotation =
                Quaternion.Euler(_rotator.transform.rotation.eulerAngles + (Vector3.forward * -_rotationSpeed * Time.deltaTime));
        }
    }

    /// <summary>
    /// Updates the teamColor of this planet
    /// </summary>
    public override void UpdateTeamColor()
    {
        base.UpdateTeamColor();
        _rotatorGlow.color = Utility.GetTeamColor(team);
        var color = Utility.GetTeamColor(team);
        color.a = 0.15f;
        _particleSystemModule.startColor = color;
    }

    /// <summary>
    /// Removes a ship from the planet when the shield is not up, otherwise remove health from the shield
    /// </summary>
    /// <param name="ship"></param>
    public override void RemovePower(float amount)
    {
        base.RemovePower(amount);
    }

    /// <summary>
    /// Sets all of the basic properties and values of the planet
    /// </summary>
    protected override void SetStats()
    {
        base.SetStats();

        switch (size)
        {
            case PlanetSize.Tiny:
                range = 1f;
                break;
            case PlanetSize.Small:
                range = 1.5f;
                break;
            default:
            case PlanetSize.Normal:
                range = 2f;
                break;
            case PlanetSize.Large:
                range = 2.5f;
                break;
            case PlanetSize.Huge:
                range = 3f;
                break;
            case PlanetSize.Gigantic:
                range = 4f;
                break;
        }

        GetComponent<CircleCollider2D>().radius = 0.4f;
        _chargeRate = CHARGE_RATE * 0.7f;
        _maxPower = MAX_POWER;
    }

    private void OnDrawGizmos()
    {
        if (_teamGlowRing)
        {
            Gizmos.color = _teamGlowRing.color;
            Gizmos.DrawWireSphere(transform.position, range);
        }
    }
}
