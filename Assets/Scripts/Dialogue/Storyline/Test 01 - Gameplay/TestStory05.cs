﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestStory05 : ActionControllerBase
{
    /// <summary>
    /// Campaign Mission 01: The Start of a Rebellion
    /// </summary>
    protected override void InitCinematicActions()
    {
        _cinematicActions.Add(new CinematicActionScreenShake(1f, 0f, true));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "In this scenario you and the opponent both have a <color='red'>Mercenary</color> planet."));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "The <color='red'>Mercenary</color> planet does not generate ships. It acts as a defensive turret, shooting at hostile ships that come in range."));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "The range and attack speed of the planet are based on the planet's size."));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "Good luck and don't march your forces to their demise!"));
    }
}
