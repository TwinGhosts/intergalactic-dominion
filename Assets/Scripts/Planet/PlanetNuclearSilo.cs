﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using MEC;
using System.Collections.Generic;

public class PlanetNuclearSilo : PlanetBehaviour
{
    private GameObject _missilePrefab;
    private int chargeAmount = 0;
    private int chargeMax = 100;
    private float damagePercentage = 15f;
    private float chargeTime = 10f;
    private float timeBeforeReset = 5f;
    [SerializeField]
    private Slider chargeBar;
    [SerializeField]
    private Image[] chargeBarImages;

    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();
        SetStats();
    }

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        _missilePrefab = Utility.GameController.planetNuclearSiloMisslePrefab;
        Timing.RunCoroutine(Charge());
    }

    public override void UpdateTeamColor()
    {
        base.UpdateTeamColor();
        chargeAmount = 0;
        foreach (var image in chargeBarImages)
        {
            var color = Utility.GetTeamColor(team);
            image.color = new Color(color.r, color.g, color.b, image.color.a);
        }
    }

    public override void Update()
    {
        if (GameController.GameState == GameStates.Playing)
        {
            base.Update();
            chargeBar.value = chargeAmount;
        }
    }

    /// <summary>
    /// Powerup the silo for a nuclear missle launch
    /// </summary>
    /// <returns></returns>
    private IEnumerator<float> Charge()
    {
        while (GameController.GameState != GameStates.Playing)
        {
            yield return Timing.WaitForSeconds(0.4f);
        }

        // Charge the silo when not fully charged
        if (chargeAmount < chargeMax)
        {
            yield return Timing.WaitForSeconds(chargeTime / chargeMax);
            if (team != Team.Neutral)
            {
                chargeAmount++;
            }
            else
            {
                chargeAmount = 0;
            }
            Timing.RunCoroutine(Charge());
        }
        // When fully charged, start launching a missle when possible
        else
        {
            yield return Timing.WaitForSeconds(timeBeforeReset / 2f);
            while (GameController.GameState != GameStates.Playing)
            {
                yield return Timing.WaitForSeconds(0.4f);
            }
            Timing.RunCoroutine(Launch());
            yield return Timing.WaitForSeconds(timeBeforeReset / 2f);
            chargeAmount = 0;
            yield return Timing.WaitForSeconds(chargeTime / chargeMax);
            Timing.RunCoroutine(Charge());
        }
    }

    /// <summary>
    /// Creates and Launches a missle to a hostile planet
    /// </summary>
    /// <returns></returns>
    private IEnumerator<float> Launch()
    {
        var missile = Instantiate(_missilePrefab);
        var missileScript = missile.GetComponent<MissileSiloScript>();
        Planet target = null;
        var hostilePlanets = StageControl.HostilePlanetList(team);

        if (hostilePlanets.Count > 0)
        {
            target = hostilePlanets[(int)Random.Range(0f, hostilePlanets.Count - 1)];
        }

        missileScript.SetData(target, team, damagePercentage);
        missile.transform.position = transform.position;
        yield return Timing.WaitForOneFrame;
    }

    /// <summary>
    /// Sets all of the basic properties and values of the planet
    /// </summary>
    protected override void SetStats()
    {
        base.SetStats();

        switch (size)
        {
            case PlanetSize.Tiny:
                damagePercentage = 10f;
                break;
            case PlanetSize.Small:
                damagePercentage = 17.5f;
                break;
            default:
            case PlanetSize.Normal:
                damagePercentage = 25f;
                break;
            case PlanetSize.Large:
                damagePercentage = 33f;
                break;
            case PlanetSize.Huge:
                damagePercentage = 40f;
                break;
            case PlanetSize.Gigantic:
                damagePercentage = 55f;
                break;
        }

        GetComponent<CircleCollider2D>().radius = 0.4f;
        _chargeRate = CHARGE_RATE;
        _maxPower = MAX_POWER;
    }
}
