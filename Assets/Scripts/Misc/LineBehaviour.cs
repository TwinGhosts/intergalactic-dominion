﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LineBehaviour : MonoBehaviour
{
    private float _progress = 0f;
    private float _connectionSpeed = 1f;

    private bool _isConnecting = false;
    private bool _isDisconnecting = false;

    [SerializeField]
    private LineRenderer _line;
    [SerializeField]
    private LineRenderer _arc;
    [SerializeField]
    private SpriteRenderer _laserBaseImage;
    [SerializeField]
    private ParticleSystem _particleSystem;

    private Planet _startingPlanet, _endPlanet;

    private Vector2 _currentPosition;

    private void Update()
    {
        // Has to have a startingPlanet to connect
        if (_startingPlanet && _endPlanet)
        {
            // Connect
            if (_isConnecting && !_isDisconnecting && _progress < 1f)
            {
                _progress += _connectionSpeed * Time.deltaTime;
            }

            // Let the line retreat back towards the startingPlanet
            if (!_isConnecting && _isDisconnecting && _progress > 0f)
            {
                _progress -= _connectionSpeed * 1.75f * Time.deltaTime;
            }
            // When the line has retreated back to the startingPlanet, delete it
            else if (!_isConnecting && _isDisconnecting && _progress <= 0f)
            {
                Destroy(gameObject);
            }

            _currentPosition = Vector2.Lerp(_startingPlanet.transform.position, _endPlanet.transform.position, _progress);
            _line.SetPosition(1, _currentPosition);

            Vector3[] linePoints = new Vector3[_arc.positionCount];
            var index = 0;
            var distance = (_endPlanet.transform.position - _startingPlanet.transform.position).normalized;
            foreach (var point in linePoints)
            {
                linePoints[index] = Vector3.Lerp(_startingPlanet.transform.position, _currentPosition, (1f / linePoints.Length) * index);
                _arc.SetPosition(index, linePoints[index]);
                index++;
            }
        }
    }

    public bool IsConnected
    {
        get
        {
            return _progress >= 1f;
        }
    }

    public void Connect(Planet startingPlanet, Planet endPlanet, float connectionSpeed)
    {
        _connectionSpeed = connectionSpeed;
        _startingPlanet = startingPlanet;
        _endPlanet = endPlanet;

        _line.SetPosition(0, _startingPlanet.transform.position);
        _line.SetPosition(1, _startingPlanet.transform.position);
        _arc.SetPosition(0, _startingPlanet.transform.position);
        _arc.SetPosition(1, _startingPlanet.transform.position);

        var color = Utility.GetTeamColor(startingPlanet.team);
        _line.startColor = color;
        _line.endColor = color;
        _arc.startColor = color;
        _arc.endColor = color;
        //_laserBaseImage.color = color;

        var psMain = _particleSystem.main;
        psMain.startColor = color;

        _isConnecting = true;
        _isDisconnecting = false;
    }

    public void Disconnect()
    {
        _isConnecting = false;
        _isDisconnecting = true;
    }
}
