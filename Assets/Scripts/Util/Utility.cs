﻿using System.Collections.Generic;
using System;
using UnityEngine;

public static class Utility
{
    public static bool planetsActive = false;
    public static Planet planetAtMouse = null;
    public static float gameSpeed = 1f;
    public static Team playerTeam = Team.Team1;

    private static Vector2 _sliceStartPosition = Vector2.zero;
    private static Vector2 _sliceCurrentPosition = Vector2.zero;
    private static bool _isPlacingSlice = false;
    private static GameObject _sliceLineObject;

    public static GameController GameController;

    public static void InputChecker()
    {
        // Create the premade slice object
        if (!_sliceLineObject)
        {
            _sliceLineObject = DrawLine(_sliceStartPosition, _sliceCurrentPosition, Color.yellow);
            _sliceLineObject.SetActive(false);
        }

        // Move the Selection Circle to the planet that is selected
        if (SelectedPlanet != null && (Vector2)GameController.transform.position != (Vector2)SelectedPlanet.transform.position)
        {
            ActivateSelectionCircle(SelectedPlanet);
        }

        #region Selection

        // Start mouse selection drag
        if (Input.GetMouseButtonDown(0) && MouseOnPlanet && !_isPlacingSlice && !SelectedPlanet && planetAtMouse.team == playerTeam)
        {
            SelectedPlanet = planetAtMouse;

            _sliceLineObject.SetActive(true);
            _sliceStartPosition = SelectedPlanet.transform.position;
            _sliceCurrentPosition = _sliceStartPosition;

            _sliceLineObject.GetComponent<LineRenderer>().startColor = Color.green;
            _sliceLineObject.GetComponent<LineRenderer>().endColor = Color.green;
            _sliceLineObject.GetComponent<LineRenderer>().SetPosition(0, _sliceStartPosition);
        }

        // Start mouse selection drag
        if (Input.GetMouseButton(0) && !_isPlacingSlice && SelectedPlanet)
        {
            _sliceCurrentPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            _sliceLineObject.GetComponent<LineRenderer>().SetPosition(1, _sliceCurrentPosition);
        }

        // Start mouse selection drag
        if (Input.GetMouseButtonUp(0) && MouseOnPlanet && !_isPlacingSlice && SelectedPlanet)
        {
            if (SelectedPlanet == planetAtMouse)
            {
                SelectedPlanet = null;
                DeactivateSelectionCircle();
                _sliceLineObject.SetActive(false);
                return;
            }

            _sliceLineObject.GetComponent<LineRenderer>().SetPosition(1, _sliceCurrentPosition);

            SelectedPlanet.AttackTarget(SelectedPlanet, planetAtMouse);

            SelectedPlanet = null;
            DeactivateSelectionCircle();
            _sliceLineObject.SetActive(false);
        }

        #endregion

        #region Deselect

        // Deselect
        if ((Input.GetMouseButtonUp(0) && !MouseOnPlanet) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
        {
            SelectedPlanet = null;
            DeactivateSelectionCircle();
            _sliceLineObject.SetActive(false);
        }

        #endregion

        #region Slice Connection

        // Start Slice Position
        if (Input.GetMouseButtonDown(0) && !MouseOnPlanet && !_isPlacingSlice)
        {
            SelectedPlanet = null;
            _sliceLineObject.GetComponent<LineRenderer>().startColor = Color.yellow;
            _sliceLineObject.GetComponent<LineRenderer>().endColor = Color.yellow;
            _sliceLineObject.SetActive(true);
            _isPlacingSlice = true;
            _sliceStartPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            _sliceCurrentPosition = _sliceStartPosition;
            _sliceLineObject.GetComponent<LineRenderer>().SetPosition(0, _sliceCurrentPosition);
        }

        // Draw the connection and move it
        if (Input.GetMouseButton(0) && _isPlacingSlice)
        {
            _sliceCurrentPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            _sliceLineObject.GetComponent<LineRenderer>().SetPosition(1, _sliceCurrentPosition);
        }

        // Cut a connection when possible
        if (Input.GetMouseButtonUp(0) && _isPlacingSlice)
        {
            _isPlacingSlice = false;
            List<PlanetConnectionKey> _tempKeyList = new List<PlanetConnectionKey>();

            foreach (var connectionDictInfo in StageControl.ConnectionInfo.Keys)
            {
                if (connectionDictInfo.fromPlanet.team == playerTeam)
                {
                    var startingPlanet = connectionDictInfo.fromPlanet.transform.position;
                    var endPlanet = connectionDictInfo.toPlanet.transform.position;

                    if (GetLineIntersection(_sliceStartPosition, _sliceCurrentPosition, startingPlanet, endPlanet))
                    {
                        _tempKeyList.Add(connectionDictInfo);
                    }
                }
            }

            foreach (var key in _tempKeyList)
            {
                PlanetConnectionInfo _pInfo;
                StageControl.ConnectionInfo.TryGetValue(key, out _pInfo);
                _pInfo.startingPlanet.BreakConnection(key.fromPlanet, key.toPlanet);
            }

            _sliceLineObject.SetActive(false);
        }

        #endregion
    }

    public static void RemoveFromDict(Dictionary<PlanetConnectionKey, PlanetConnectionInfo> _dictOne, PlanetConnectionKey key)
    {
        _dictOne.Remove(key);
        StageControl.ConnectionInfo.Remove(key);
    }

    /// <summary>
    /// Increases the gameSpeed by a factor of 2 or when it reaches !2 it will reset to 1
    /// </summary>
    public static void IncreaseGameSpeed()
    {
        if (gameSpeed * gameSpeed < 5f)
        {
            gameSpeed *= gameSpeed;
        }
        else
        {
            gameSpeed = 1f;
        }

        Time.timeScale = gameSpeed;
    }

    /// <summary>
    /// Returns the cinematicController for usage
    /// </summary>
    /// <returns></returns>
    public static CinematicController GetCinematicController()
    {
        return GameController.GetComponentInParent<CinematicController>();
    }

    /// <summary>
    /// Goes from Cinematic mode to Playing mode and disables the dialogueBox
    /// </summary>
    public static void ResetCinematicModeToPlaying()
    {
        GameController.GameState = GameStates.Playing;
        GameController.DialogueManager.CloseDialogueBox();
        GameController.CinematicPanel.Close();
    }

    /// <summary>
    /// Gets the game controller gameobject.
    /// </summary>
    /// <returns>The game controller.</returns>
    public static GameObject GetGameControllerObject()
    {
        return GameController.gameObject;
    }

    /// <summary>
    /// Deactivates the selection circle.
    /// </summary>
    public static void DeactivateSelectionCircle()
    {
        var sCircle = GameController.SelectionCircle;
        sCircle.transform.position = Vector2.one * 9999f;
    }

    /// <summary>
    /// Activates the selection circle.
    /// </summary>
    public static void ActivateSelectionCircle(Planet target)
    {
        var sCircle = GameController.SelectionCircle;
        sCircle.transform.position = target.transform.position;
        sCircle.transform.localScale = target.transform.localScale;
    }

    /// <summary>
    /// returns the wincondition.
    /// </summary>
    /// <returns><c>true</c>, if condition was windowed, <c>false</c> otherwise.</returns>
    public static bool PlayerHasWon(Team team)
    {
        if (!planetsActive)
            return false;

        return StageControl.HostilePlanetList(playerTeam).Count == 0;
    }

    /// <summary>
    /// Loses the condition.
    /// </summary>
    /// <returns><c>true</c>, if condition was lost, <c>false</c> otherwise.</returns>
    public static bool LoseCondition()
    {
        if (!planetsActive)
            return false;

        return StageControl.FactionPlanetCount(playerTeam) == 0;
    }

    /// <summary>
    /// Gets the color of the team.
    /// </summary>
    /// <returns>The team color.</returns>
    /// <param name="team">Team.</param>
    public static Color GetTeamColor(Team team)
    {
        Color[] teamColors = GameController.gameObject.GetComponent<TeamColors>().teamColors;
        return teamColors[(int)team];
    }

    /// <summary>
    /// Gets or sets the selected planet.
    /// </summary>
    /// <value>The selected planet.</value>
    public static Planet SelectedPlanet
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="Utility"/> mouse on planet.
    /// </summary>
    /// <value><c>true</c> if mouse on planet; otherwise, <c>false</c>.</value>
    public static bool MouseOnPlanet
    {
        get;
        set;
    }

    /// <summary>
    /// Gets a value indicating whether this <see cref="Utility"/> is selected.
    /// </summary>
    /// <value><c>true</c> if selected; otherwise, <c>false</c>.</value>
    public static bool Selected
    {
        get { return SelectedPlanet != null; }
    }

    public static Planet PlanetFromMousePosition()
    {
        if (MouseOnPlanet)
        {
            return planetAtMouse;
        }
        else
        {
            return null;
        }
    }

    public static GameObject CreateLaser(Planet startingPlanet, Planet targetPlanet)
    {
        var lineObject = GameObject.Instantiate(GameController._laserPrefab, startingPlanet.GetComponentInParent<Canvas>().transform);
        lineObject.transform.position = targetPlanet.transform.position;

        //lineObject.transform.position = startingPlanet.transform.position;
        //lineObject.AddComponent<LineRenderer>();
        /*lineObject.AddComponent<LineBehaviour>();

        LineRenderer lr = lineObject.GetComponent<LineRenderer>();

        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));

        lr.startColor = GetTeamColor(startingPlanet.team);
        lr.endColor = GetTeamColor(startingPlanet.team);

        lr.startWidth = 0.015f * (((int)startingPlanet.size + 1) / 2f);
        lr.endWidth = 0.015f * (((int)startingPlanet.size + 1) / 2f);

        lr.SetPosition(0, startingPlanet.transform.position);
        lr.SetPosition(1, startingPlanet.transform.position);*/

        return lineObject;
    }

    public static GameObject DrawLine(Vector3 start, Vector3 end, Color color)
    {
        GameObject lineObject = new GameObject();

        lineObject.transform.position = start;
        lineObject.AddComponent<LineRenderer>();

        LineRenderer lr = lineObject.GetComponent<LineRenderer>();

        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));

        lr.startColor = color;
        lr.endColor = color;

        lr.startWidth = 0.025f;
        lr.endWidth = 0.025f;

        lr.SetPosition(0, start);
        lr.SetPosition(1, end);

        return lineObject;
    }

    public static void DrawLineTimed(Vector3 start, Vector3 end, Color color, float duration)
    {
        GameObject lineObject = new GameObject();

        lineObject.transform.position = start;
        lineObject.AddComponent<LineRenderer>();

        LineRenderer lr = lineObject.GetComponent<LineRenderer>();

        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));

        lr.startColor = color;
        lr.endColor = color;

        lr.startWidth = 0.025f;
        lr.endWidth = 0.025f;

        lr.SetPosition(0, start);
        lr.SetPosition(1, end);

        GameObject.Destroy(lineObject, duration);
    }

    public static MusicManager musicManager = null;
    public static bool musicIsPlaying = false;

    public static bool GetLineIntersection(Vector2 A, Vector2 B, Vector2 C, Vector2 D) // TODO FIX GET LINE INTERSECTION
    {
        // Get both vector directions
        var r = B - A;
        var s = D - C;

        // A + t * r = C + u * s
        // t = scalar for AB
        // u = scalar for CD

        // 2 Unknowns -> 2D cross products
        // A cross B = A.x * B.y - A.y * B.x
        // because 2D cross product returns 0 when the equation is the same
        var d = r.x * s.y - r.y * s.x;
        var u = ((C.x - A.x) * r.y - (C.y - A.y) * r.x) / d;
        var t = ((C.x - A.x) * s.y - (C.y - A.y) * s.x) / d;

        return (0 <= u && u <= 1 && 0 <= t && t <= 1)/* && A + t * r*/;
    }
}

/// <summary>
/// The teams
/// </summary>
public enum Team
{
    Neutral,
    Team1,
    Team2,
    Team3,
    Team4,
    Team5,
    Team6,
}

public enum PlanetSize
{
    Tiny,
    Small,
    Normal,
    Large,
    Huge,
    Gigantic,
}

public enum PlanetType
{
    Normal,
    Guardian,
    Regenerator,
    Nuclear,
    Mercenary,
    Disruptor,
}

public enum ShipStates
{
    Idle,
    Attacking,
}

public enum ShipType
{
    Sprinter,
    Guardian,
    Conqueror,
}

public enum GameStates
{
    Playing,
    Paused,
    Cinematic,
    Win,
    Lose,
}