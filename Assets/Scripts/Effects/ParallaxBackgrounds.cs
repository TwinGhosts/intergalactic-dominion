﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBackgrounds : MonoBehaviour
{
    [SerializeField]
    private List<BackgroundHolder> _backgroundLayers = new List<BackgroundHolder>();
    private bool _runOnce = true;

    // Update is called once per frame
    private void Update()
    {
        // Translate each background per layer
        var motionSpeed = 0.9f;
        var motionFallOff = 0.125f;
        var cameraTransform = Camera.main.transform;
        var index = 10;

        foreach (var backgroundLayer in _backgroundLayers)
        {
            foreach (var background in backgroundLayer.Backgrounds)
            {
                if (background != null)
                {
                    background.position = new Vector3(cameraTransform.position.x * motionSpeed, cameraTransform.position.y * motionSpeed, 10f);
                    if (_runOnce)
                    {
                        foreach (var child in background.GetComponentsInChildren<SpriteRenderer>())
                        {
                            child.sortingOrder = index;
                        }
                    }
                }
            }
            motionSpeed -= motionFallOff;
            motionSpeed = Mathf.Clamp(motionSpeed, 0.001f, 15f);
            index--;
        }

        _runOnce = false;
    }
}

[System.Serializable]
public class BackgroundHolder
{
    [SerializeField]
    private List<Transform> _backgrounds = new List<Transform>();

    public List<Transform> Backgrounds
    {
        get
        {
            return _backgrounds;
        }
    }
}
