﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestStory08 : ActionControllerBase
{
    /// <summary>
    /// Campaign Mission 01: The Start of a Rebellion
    /// </summary>
    protected override void InitCinematicActions()
    {
        _cinematicActions.Add(new CinematicActionScreenShake(1f, 0f, true));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "The final scenario of this test!"));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "In this test you will face an opponent with an allied commander."));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "Allies cannot attack each other and if you were to both capture a planet at roughly the same time, then your ships will be sent back to the planet where they came from."));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "Good luck and support your ally!"));
    }
}
