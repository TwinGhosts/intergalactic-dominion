﻿using UnityEngine;

public class PlanetRegenerator : PlanetBehaviour
{
    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();
        SetStats();
    }

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
    }

    /// <summary>
    /// Removes a ship from the planet when the shield is not up, otherwise remove health from the shield
    /// </summary>
    /// <param name="ship"></param>
    public override void RemovePower(float amount)
    {
        base.RemovePower(amount);
    }

    /// <summary>
    /// Sets all of the basic properties and values of the planet
    /// </summary>
    protected override void SetStats()
    {
        base.SetStats();
        GetComponent<CircleCollider2D>().radius = 0.4f;
        _chargeRate = CHARGE_RATE * 0.7f;
        _maxPower = MAX_POWER;
    }
}
