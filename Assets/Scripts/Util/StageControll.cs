﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

public static class StageControl
{
    private static List<List<Planet>> _planetsPerTeam = new List<List<Planet>>();
    private static List<Faction> _factions = new List<Faction>();
    private static Dictionary<PlanetConnectionKey, PlanetConnectionInfo> _connectionInfo = new Dictionary<PlanetConnectionKey, PlanetConnectionInfo>(new PlanetConnectionKey.EqualityComparer());
    private static List<Planet> _allPlanets = new List<Planet>();

    public static List<Planet> AllPlanets
    {
        get
        {
            if (_allPlanets.Count != 0)
            {
                return _allPlanets;
            }
            else
            {
                for (int i = 0; i < 7; i++)
                {
                    foreach (var planet in _planetsPerTeam[i])
                    {
                        _allPlanets.Add(planet);
                    }
                }

                return _allPlanets;
            }
        }
    }

    public static Dictionary<PlanetConnectionKey, PlanetConnectionInfo> ConnectionInfo
    {
        get
        {
            return _connectionInfo;
        }
    }

    #region Initialization
    public static void Init(List<Faction> factions)
    {
        _planetsPerTeam.Clear();
        _factions.Clear();
        _connectionInfo.Clear();

        _factions = factions;

        // Clear the team lists and re-add the clean lists
        for (int i = 0; i < Enum.GetValues(typeof(Team)).Cast<Team>().ToList().Count; i++)
        {
            _planetsPerTeam.Add(new List<Planet>());
        }
    }
    #endregion

    #region Planet management
    public static List<Planet> TeamPlanetList(Team team)
    {
        return _planetsPerTeam[(int)team];
    }

    public static int TeamPlanetCount(Team team)
    {
        return _planetsPerTeam[(int)team].Count;
    }

    public static void AddPlanetToTeam(Team team, Planet planet)
    {
        _planetsPerTeam[(int)team].Add(planet);
    }

    public static int FactionPlanetCount(Team team)
    {
        var count = 0;
        count += TeamPlanetCount(team);

        if (GetAlliesForTeam(team) != null)
        {
            foreach (var _team in GetAlliesForTeam(team))
            {
                count += TeamPlanetCount(team);
            }
        }

        return count;
    }

    public static List<Planet> HostilePlanetList(Team team)
    {
        var _planets = new List<Planet>();
        var _enemyTeams = new List<Team>(GetEnemiesForTeam(team));

        foreach (var _team in _enemyTeams)
        {
            foreach (var _teamPlanet in TeamPlanetList(_team))
            {
                if (_teamPlanet)
                {
                    _planets.Add(_teamPlanet);
                }
            }
        }

        return _planets;
    }

    public static List<Planet> FactionPlanetList(Team team)
    {
        var _planets = new List<Planet>();
        var _teams = new List<Team>(GetAlliesForTeam(team))
        {
            team
        };

        foreach (var _team in _teams)
        {
            foreach (var _teamPlanets in TeamPlanetList(_team))
            {
                _planets.Add(_teamPlanets);
            }
        }

        Debug.Log(_planets.Count);
        return _planets;
    }

    public static void RemovePlanetFromTeam(Team team, Planet planet = null)
    {
        if (planet)
        {
            _planetsPerTeam[(int)team].Remove(planet);
        }
        else if (_planetsPerTeam[(int)team].Count != 0)
        {
            _planetsPerTeam[(int)team].RemoveAt(_planetsPerTeam[(int)team].Count - 1);
        }

        Debug.LogError("No planet found to remove?");
    }

    public static void SwitchPlanetOwner(Planet attacker, Planet switcher)
    {
        switcher.BreakAllConnections();
        switcher.BreakAttackingConnections();

        if (switcher.team == attacker.team)
        {
            Debug.LogError("Planet is already on the new team: " + switcher.team);
        }
        else
        {
            _planetsPerTeam[(int)switcher.team].Remove(switcher);
            switcher.team = attacker.team;
            _planetsPerTeam[(int)switcher.team].Add(switcher);
        }
    }
    #endregion

    #region Allied Teams
    public static List<Team> GetEnemiesForTeam(Team team)
    {
        var _tempTeam = Enum.GetValues(typeof(Team)).Cast<Team>().ToList();
        var _allies = GetAlliesForTeam(team);

        _tempTeam.Remove(team);

        if (_allies != null)
        {
            for (int j = _tempTeam.Count - 1; j >= 0; j--)
            {
                var _team = _tempTeam[j];
                for (int i = _allies.Length - 1; i >= 0; i--)
                {
                    var _ally = _allies[i];
                    if (_team == _ally)
                    {
                        _tempTeam.Remove(_ally);
                    }
                }
            }
        }

        return _tempTeam;
    }

    public static Team[] GetAlliesForTeam(Team team)
    {
        foreach (var f in _factions)
        {
            if (f.Team == team)
            {
                return f.Allies;
            }
        }

        return null;
    }
    #endregion

    public static bool TeamIsAllyOf(Team team, Team possibleAlly)
    {
        var _allies = GetAlliesForTeam(team);

        if (_allies != null)
        {
            foreach (var _ally in _allies)
            {
                if (possibleAlly == _ally)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public static string PrintDebugEnemies(Team team)
    {
        var _enemies = GetEnemiesForTeam(team);
        string _text = "Enemies for " + team.ToString() + " |";

        foreach (var _enemy in _enemies)
        {
            _text += _enemy.ToString() + " | ";
        }

        Debug.Log(_text);
        return _text;
    }

    public static string PrintDebugAllies(Team team)
    {
        var _allies = GetAlliesForTeam(team);
        string _text = "Allies for " + team.ToString() + " |";

        foreach (var _ally in _allies)
        {
            _text += _ally.ToString() + " | ";
        }

        Debug.Log(_text);
        return _text;
    }
}

[System.Serializable]
#region Faction Class
public class Faction
{
    [SerializeField]
    private Team _team;
    [SerializeField]
    private Team[] _allies;

    public Team Team
    {
        get
        {
            return _team;
        }
    }

    public Team[] Allies
    {
        get
        {
            return _allies;
        }
    }
}
#endregion
