﻿using UnityEngine;

public class PauseCanvas : MonoBehaviour
{
    public GameObject pauseCanvas;
    private bool paused;

    public void UnPause()
    {
        pauseCanvas.SetActive(false);
        Time.timeScale = Utility.gameSpeed;
        paused = !paused;
    }

    public void Pause()
    {
        pauseCanvas.SetActive(true);
        Time.timeScale = 0f;
        paused = !paused;
    }

    private void Update()
    {
        // TODO Change to mobile phone stuff
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (paused)
            {
                UnPause();
            }
            else
            {
                Pause();
            }
        }
    }
}
