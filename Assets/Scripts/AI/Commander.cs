﻿using UnityEngine;
using UnityEngine.Events;

public class Commander : MonoBehaviour
{
    [Header("Commander")]
    [SerializeField]
    private Character _commander = Character.UNKNOWN;
    [SerializeField]
    private Team _team = Team.Team1;

    [Header("Action Callbacks")]
    public UnityEvent OnPlanetOrder;
    public UnityEvent OnPlanetAttackOrder;
    public UnityEvent OnPlanetSupportOrder;
    public UnityEvent OnPlanetTakeOver;

    public Character Character
    {
        get
        {
            return _commander;
        }
    }

    public Team Team
    {
        get
        {
            return _team;
        }
    }
}
