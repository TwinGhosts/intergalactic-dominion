﻿using System.Collections.Generic;
using MEC;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CharacterImageHolder))]
public class DialogueManager : MonoBehaviour
{
    [SerializeField]
    private CinematicController _cinematicController;

    [Header("Dialogue Box Properties")]
    [SerializeField]
    private DialogueDirection _currentDialogueDirection = DialogueDirection.Left;

    [Header("Base Dialogue Box References")]
    [SerializeField]
    private Transform _dialogueBox;

    [SerializeField]
    private AnimationCurve _openCurve;
    [SerializeField]
    private AnimationCurve _closeCurve;

    [Header("Left Box References")]
    [SerializeField]
    private Image _leftCharacterImageBox;
    [SerializeField]
    private Image _leftCharacterImage;
    [SerializeField]
    private Text _leftCharacterNameText;
    [SerializeField]
    private Text _leftTextFieldText;
    private string _previousCharacterName = "";

    [Header("Right Box References")]
    [SerializeField]
    private Image _rightCharacterImageBox;
    [SerializeField]
    private Image _rightCharacterImage;
    [SerializeField]
    private Text _rightCharacterNameText;
    [SerializeField]
    private Text _rightTextFieldText;

    private Image _currentCharacterImage;
    private Text _currentCharacterNameText;
    private Text _currentTextFieldText;

    private CharacterImageHolder imageHolder;

    private string _currentDialogue = "";
    private string _dialogueToSpeak = "";

    private float _openHeight;
    private float _closeHeight;

    private bool _isSpeaking = false;
    private bool _isOpen = false;
    private float _boxAnimationSpeed = 1.9f;

    private bool _isOpening = false;
    private bool _isClosing = false;

    private bool _isTypingName = false;

    private float _progress = 0f;

    private float _defaultTypingSpeed = 0.025f;

    private IEnumerator<float> _lastFeedRoutine;

    private void Awake()
    {
        imageHolder = GetComponent<CharacterImageHolder>();

        // Set the basic Element references
        var rectTransform = _dialogueBox.GetComponent<RectTransform>();
        _openHeight = rectTransform.anchoredPosition.y;
        _closeHeight = rectTransform.anchoredPosition.y - rectTransform.sizeDelta.y * 1.25f;
        _dialogueBox.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, _closeHeight);

        UpdateElementReferences();
    }

    private void Update()
    {
        // TODO Change for android
        if (Input.GetKeyDown(KeyCode.Return) && GameController.GameState == GameStates.Cinematic)
        {
            if (_isSpeaking)
            {
                SkipDialogue();
            }
            else
            {
                _cinematicController.OnActionFinished.Invoke();
            }
        }
    }

    /// <summary>
    /// Skips the remaining text to type and spawns all of the text at once, finishing the dialogue
    /// </summary>
    public void SkipDialogue()
    {
        if (_isOpening)
        {
            _progress = 1f;
            _dialogueBox.GetComponent<RectTransform>().position = new Vector2(0f, _openHeight);
            _isOpen = true;
        }
        else if (_isClosing)
        {
            _progress = 0f;
            _dialogueBox.GetComponent<RectTransform>().position = new Vector2(0f, _closeHeight);
            _isOpen = false;
        }

        if (_isSpeaking)
        {
            Timing.KillCoroutines("CINEMATIC_TEXT");
            _isTypingName = false;
            _isSpeaking = false;
            _currentDialogue = _dialogueToSpeak;
            _currentTextFieldText.text = _currentDialogue;
        }
        else
        {
            Debug.LogError("Dialogue Error > No dialogue is being spoken!");
        }
    }

    /// <summary>
    /// Start sending the text to the dialogueBoxes
    /// Shows the images of the character that is speaking
    /// Shows the name of the character
    /// Opens the window only when it isnt yet opened
    /// </summary>
    /// <param name="dialogueDirection"></param>
    /// <param name="character"></param>
    /// <param name="dialogue"></param>
    /// <returns></returns>
    public IEnumerator<float> SendDialogue(DialogueDirection dialogueDirection, Character character, string dialogue)
    {
        var check = false;
        var index = 0;

        // Only continue when the game was active
        if (GameController.GameState == GameStates.Playing || GameController.GameState == GameStates.Cinematic)
        {
            check = true;
        }

        if (check)
        {
            GameController.GameState = GameStates.Cinematic;

            // Choose which side to activate based on the given direction
            var oldDirection = _currentDialogueDirection;
            _currentDialogueDirection = dialogueDirection;
            _currentDialogue = "";
            _currentTextFieldText.text = "";
            _currentCharacterImage.sprite = imageHolder.GetCharacterImage(Character.NONE);

            ActivateBoxSide(dialogueDirection);
            UpdateElementReferences();

            // Open the box first before sending text
            if (!_isOpen)
            {
                yield return Timing.WaitUntilDone(Timing.RunCoroutine(Open()));
            }

            _currentCharacterImage.sprite = imageHolder.GetCharacterImage(character);
            yield return Timing.WaitUntilDone(Timing.RunCoroutine(ShowName(character, oldDirection)));
            _dialogueToSpeak = dialogue;

            _isSpeaking = true;

            // Add letters to the textbox
            while (_currentDialogue.Length < _dialogueToSpeak.Length)
            {
                _currentDialogue += _dialogueToSpeak[index];
                _currentTextFieldText.text = _currentDialogue;
                index += 1;
                yield return Timing.WaitForSeconds(_defaultTypingSpeed);
            }

            _isSpeaking = false;
        }

        yield return Timing.WaitForOneFrame;
    }

    /// <summary>
    /// Opens the dialogueBox with an animation
    /// </summary>
    /// <returns></returns>
    private IEnumerator<float> Open()
    {
        _isOpening = true;

        _currentDialogue = "";
        _currentCharacterNameText.text = "";
        _currentCharacterImage.sprite = imageHolder.GetCharacterImage(Character.NONE);

        while (_progress < 1f)
        {
            _dialogueBox.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(new Vector2(0f, _closeHeight), new Vector2(0f, _openHeight), _openCurve.Evaluate(_progress));
            _progress += Time.deltaTime * _boxAnimationSpeed;
            yield return Timing.WaitForOneFrame;
        }

        _isOpen = true;
        _isOpening = false;
    }

    /// <summary>
    /// Closes the dialogueBox with an animation
    /// </summary>
    /// <returns></returns>
    private IEnumerator<float> Close()
    {
        _isClosing = true;

        ActivateBoxSide(_currentDialogueDirection);

        _currentDialogue = "";
        _currentCharacterNameText.text = "";
        _currentCharacterImage.sprite = imageHolder.GetCharacterImage(Character.NONE);

        while (_progress > 0f)
        {
            _dialogueBox.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(new Vector2(0f, _openHeight), new Vector2(0f, _closeHeight), _closeCurve.Evaluate(1f - _progress));
            _progress -= Time.deltaTime * _boxAnimationSpeed;
            yield return Timing.WaitForOneFrame;
        }

        GameController.GameState = GameStates.Playing;

        _isOpen = false;
        _isClosing = false;
    }

    /// <summary>
    /// Wrapper method to close the dialogueBox
    /// </summary>
    public void CloseDialogueBox()
    {
        Timing.RunCoroutine(Close());
    }

    /// <summary>
    /// Opens the dialogueBox with an animation
    /// </summary>
    /// <returns></returns>
    private IEnumerator<float> ShowName(Character nextCharacter, DialogueDirection nextDirection)
    {
        var index = 0;
        _isTypingName = true;

        // Reset the name boxes when switching sides
        if (_currentDialogueDirection != nextDirection)
        {
            _leftCharacterNameText.text = "";
            _rightCharacterNameText.text = "";
        }

        // Check whether the name is not the same as the current name
        if (_previousCharacterName != nextCharacter.ToString())
        {
            _currentCharacterNameText.text = "";
        }

        // When the name is different, type it
        while (_currentCharacterNameText.text.Length < nextCharacter.ToString().Length && _isTypingName)
        {
            _currentCharacterNameText.text += nextCharacter.ToString()[index];
            index++;
            yield return Timing.WaitForSeconds(_defaultTypingSpeed);
        }

        _currentCharacterNameText.text = nextCharacter.ToString();
        _previousCharacterName = nextCharacter.ToString();

        _isTypingName = false;

        yield return Timing.WaitForOneFrame;
    }

    /// <summary>
    /// Activates and deactives components based on the given direction
    /// </summary>
    /// <param name="direction"></param>
    private void ActivateBoxSide(DialogueDirection direction)
    {
        // Left side
        _leftCharacterImageBox.gameObject.SetActive((direction == DialogueDirection.Left) ? true : false);
        _leftCharacterNameText.gameObject.SetActive((direction == DialogueDirection.Left) ? true : false);
        _leftTextFieldText.gameObject.SetActive((direction == DialogueDirection.Left) ? true : false);

        // Right side
        _rightCharacterImageBox.gameObject.SetActive((direction == DialogueDirection.Left) ? false : true);
        _rightCharacterNameText.gameObject.SetActive((direction == DialogueDirection.Left) ? false : true);
        _rightTextFieldText.gameObject.SetActive((direction == DialogueDirection.Left) ? false : true);

        // Set the currents
        _currentCharacterImage = (direction == DialogueDirection.Left) ? _leftCharacterImage : _rightCharacterImage;
        _currentCharacterNameText = (direction == DialogueDirection.Left) ? _leftCharacterNameText : _rightCharacterNameText;
        _currentTextFieldText = (direction == DialogueDirection.Left) ? _leftTextFieldText : _rightTextFieldText;
    }

    /// <summary>
    /// Updates all of the elements that could be referenced for usage based on the dialogueDirection
    /// </summary>
    private void UpdateElementReferences()
    {
        _currentCharacterImage = (_currentDialogueDirection == DialogueDirection.Left) ? _leftCharacterImage : _rightCharacterImage;
        _currentCharacterNameText = (_currentDialogueDirection == DialogueDirection.Left) ? _leftCharacterNameText : _rightCharacterNameText;
        _currentTextFieldText = (_currentDialogueDirection == DialogueDirection.Left) ? _leftTextFieldText : _rightTextFieldText;
    }

    private void UpdateTextBox()
    {
        _currentTextFieldText.text = _currentDialogue;
    }
}

public enum DialogueDirection
{
    Left,
    Right,
}
