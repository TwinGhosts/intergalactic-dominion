﻿using UnityEngine;

public class CameraDistanceMovement : MonoBehaviour
{
    Vector2 centerPoint = Vector2.zero;
    Vector2 targetPoint;

    /// <summary>
    /// Play music when playing on the play button from the menu // TODO - Refactor
    /// </summary>
    public void PlayButtonClick()
    {
        Utility.musicManager.PlaySound(Utility.musicManager.musicSource, Utility.musicManager.menuOption, 0.3f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Get the mousePos
        targetPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        // Get the distance between the center point and the mousePos
        float scrollSpeed = 8f;
        Vector2 angleVector = (targetPoint - centerPoint) / scrollSpeed;

        // Finally, move the camera to the position
        Camera.main.transform.position = new Vector3(angleVector.x, angleVector.y, Camera.main.transform.position.z);
    }
}
