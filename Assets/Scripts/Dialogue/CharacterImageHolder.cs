﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterImageHolder : MonoBehaviour
{
    [SerializeField]
#pragma warning disable 0649
    private Sprite _characterZed,
        _characterTG04,
        _characterAlera,
        _characterGhost,
        _characterCretas,
        _characterSpectre,
        _characterWraith,
        _characterEratus,
        _characterEidolon,
        _characterRevenant,
        _characterEnigma,
        _characterUnknown,
        _characterNone;
#pragma warning restore 0649

    private List<Sprite> _characterImages = new List<Sprite>();

    private void Awake()
    {
        // Add all characters to a list for easy referencing
        _characterImages.Add(_characterZed);
        _characterImages.Add(_characterTG04);
        _characterImages.Add(_characterAlera);
        _characterImages.Add(_characterGhost);
        _characterImages.Add(_characterCretas);
        _characterImages.Add(_characterSpectre);
        _characterImages.Add(_characterWraith);
        _characterImages.Add(_characterEratus);
        _characterImages.Add(_characterEidolon);
        _characterImages.Add(_characterRevenant);
        _characterImages.Add(_characterEnigma);
        _characterImages.Add(_characterUnknown);
        _characterImages.Add(_characterNone);
    }

    /// <summary>
    /// Returns the image of a character based on the Character enum
    /// </summary>
    /// <param name="character"></param>
    /// <returns></returns>
    public Sprite GetCharacterImage(Character character)
    {
        return _characterImages[(int)character];
    }
}

public enum Character
{
    ZED,
    TG04,
    ALERA,
    GHOST,
    CRETAS,
    SPECTER,
    WRAITH,
    ERATUS,
    EIDOLON,
    REVENANT,
    ENIGMA,
    UNKNOWN,
    NONE,
}