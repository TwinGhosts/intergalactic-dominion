﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for usage when creating a campaign cinematic, inherit from it to get the functionality for going through the action List
/// </summary>
public class ActionControllerBase : MonoBehaviour
{
    [SerializeField]
    protected DialogueManager _dialogueManager;

    protected List<CinematicActionBase> _cinematicActions = new List<CinematicActionBase>();
    protected int _index = 0;

    protected virtual void InitCinematicActions() { }

    private void Awake()
    {
        InitCinematicActions();
    }

    protected virtual CinematicActionBase GetFirstCinematicAction()
    {
        if (_cinematicActions.Count > 0)
        {
            return _cinematicActions[0];
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Returns the next Cinematic actions for usage and increments the index
    /// </summary>
    protected virtual CinematicActionBase NextCinematicAction()
    {
        if (_index + 1 <= _cinematicActions.Count)
        {
            var actionHolder = _cinematicActions[_index];
            _index++;
            return actionHolder;
        }
        else
        {
            Utility.ResetCinematicModeToPlaying();
            return null;
        }
    }

    /// <summary>
    /// Goes to the next Cinematic Action and activates it
    /// </summary>
    public virtual void Progress()
    {
        var nextAction = NextCinematicAction();
        if (nextAction != null)
        {
            nextAction.Activate();
        }
    }

    /// <summary>
    /// Returns all of the Cinematic Actions
    /// </summary>
    public virtual List<CinematicActionBase> GetActionList
    {
        get
        {
            return _cinematicActions;
        }
    }
}