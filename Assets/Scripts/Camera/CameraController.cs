﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private float _motionSpeed = 2f;

    // Update is called once per frame
    private void Update()
    {
        if (GameController.GameState == GameStates.Playing)
        {
            var inputH = Input.GetAxis("Horizontal") * Time.deltaTime * _motionSpeed;
            var inputV = Input.GetAxis("Vertical") * Time.deltaTime * _motionSpeed;

            // TODO CLAMP CORRECTLY
            transform.position += (Vector3)new Vector2(inputH, inputV);
            transform.position =
                new Vector3(
                    Mathf.Clamp(transform.position.x, -15f, 15f),
                    Mathf.Clamp(transform.position.y, -15f, 15f),
                    0f
                    );
        }
    }
}
