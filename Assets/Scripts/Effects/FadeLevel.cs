﻿using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Fading))]
public class FadeLevel : MonoBehaviour
{
    private string scene;
    private int sceneIndex;

    /// <summary>
    /// Go to a level referenced by the sceneName with a fade effect
    /// </summary>
    /// <param name="scene"></param>
	public void ActivateLevelSwitch(string scene)
    {
        this.scene = scene;
        float fadeTime = GetComponent<Fading>().fadeSpeed * 100f * 1.25f * Time.deltaTime;

        GetComponent<Fading>().BeginFade(1);
        if (scene == "quit")
        {
            Invoke("Quit", fadeTime);
        }
        else
        {
            Invoke("SwitchLevel", fadeTime);
        }
    }

    /// <summary>
    /// Go to a level referenced by the sceneIndex with a fade effect
    /// </summary>
    /// <param name="sceneIndex"></param>
    public void ActivateLevelSwitch(int sceneIndex)
    {
        this.sceneIndex = sceneIndex;
        float fadeTime = GetComponent<Fading>().fadeSpeed * 100f * 1.25f * Time.deltaTime;

        GetComponent<Fading>().BeginFade(1);
        if (scene == "quit")
        {
            Invoke("Quit", fadeTime);
        }
        else
        {
            Invoke("NextScene", fadeTime);
        }
    }

    /// <summary>
    /// Go to the next level based on the scene index (int) with a fade effect
    /// </summary>
	public void NextLevel()
    {
        float fadeTime = GetComponent<Fading>().fadeSpeed * 100f * 1.25f * Time.deltaTime;
        sceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
        Invoke("NextScene", fadeTime);
        GetComponent<Fading>().BeginFade(1);
    }

    /// <summary>
    /// Go back to the current level (reload) with a fade effect
    /// </summary>
	public void RetryLevel()
    {
        float fadeTime = GetComponent<Fading>().fadeSpeed * 100f * 1.25f * Time.deltaTime;
        sceneIndex = SceneManager.GetActiveScene().buildIndex;
        Invoke("NextScene", fadeTime);
        GetComponent<Fading>().BeginFade(1);
    }

    /// <summary>
    /// Quit the application when not in WebGL or Playmode in the editor
    /// </summary>
	private void Quit()
    {
        Application.Quit();
    }

    /// <summary>
    /// Load another level
    /// </summary>
	private void SwitchLevel()
    {
        SceneManager.LoadScene(scene);
    }

    /// <summary>
    /// Go to the next scene relative from this index
    /// </summary>
	private void NextScene()
    {
        SceneManager.LoadScene(sceneIndex);
    }
}
