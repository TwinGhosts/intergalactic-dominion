﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

public class Test1_5_AI : MonoBehaviour
{
    [SerializeField]
    private List<Planet> _planets = new List<Planet>();

    [SerializeField]
    private float _timeUntillAttack = 8f;

    private bool _runOnce = true;

    private void Update()
    {
        if (GameController.GameState == GameStates.Playing && _runOnce)
        {
            _runOnce = false;
            Timing.RunCoroutine(AttackRoutine());
        }
    }

    private IEnumerator<float> AttackRoutine()
    {
        yield return Timing.WaitForSeconds(_timeUntillAttack);
        AttackClosest();

        Timing.RunCoroutine(AttackRoutine());
    }

    private void AttackClosest()
    {
        // Loop through all planets
        foreach (var planet1 in _planets)
        {
            if (planet1.team == Team.Team2)
            {
                var distance = Mathf.Infinity;
                Planet closestPlanet = null;

                // Let them try to attack an other planet
                foreach (var planet2 in _planets)
                {
                    if (planet2.team != Team.Team2)
                    {
                        if ((planet1.transform.position - planet2.transform.position).magnitude < distance)
                        {
                            distance = (planet1.transform.position - planet2.transform.position).magnitude;
                            closestPlanet = planet2;
                        }
                    }
                }

                // Attack the closestPlanet
                if (planet1 && closestPlanet)
                {
                    planet1.AttackTarget(closestPlanet);
                }
            }
        }
    }
}
