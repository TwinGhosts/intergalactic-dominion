﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{

    [Header("Source")]
    public AudioSource musicSource;

    [Header("Sound Files")]
    public AudioClip menuOption;
    public AudioClip order;
    public AudioClip losePlanet;
    public AudioClip winPlanet;
    public AudioClip lose;
    public AudioClip win;
    public AudioClip explode1;
    public AudioClip explode2;
    public AudioClip explode3;
    public AudioClip support;

    [Header("Music Files")]
    public AudioClip music;

    void Awake()
    {
        // Keep the object when there is no music manager yet
        if (Utility.musicManager == null)
        {
            DontDestroyOnLoad(gameObject);
            Utility.musicManager = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void PlaySound(AudioSource source, AudioClip clipToPlay, float volume)
    {
        source.PlayOneShot(clipToPlay, volume);
    }

    public void PlayMusic(AudioClip musicClip, float volume, bool repeat)
    {
        musicSource.clip = musicClip;
        musicSource.volume = volume;
        musicSource.Play();
        musicSource.loop = repeat;
    }
}
