﻿using UnityEngine;
using UnityEngine.UI;

public class ProfileManager : MonoBehaviour
{

    public GameObject confirmationLoadPanel;
    public GameObject confirmationCreatePanel;
    public GameObject confirmationDeletePanel;
    public InputField inputField;

    private int currentSaveSlotIndex = 0;

    public void LoadPlayer(int saveSlotIndex)
    {
        currentSaveSlotIndex = saveSlotIndex;

        // When the profile can be loaded, continue with the fetched data
        if (SaveLoadManager.LoadPlayer(saveSlotIndex))
        {
            confirmationLoadPanel.SetActive(true);
        }
        else // Create a new standard profile otherwise
        {
            confirmationCreatePanel.SetActive(true);
        }
    }

    public void SetCurrentSaveSlotIndex(int index)
    {
        currentSaveSlotIndex = index;
    }

    public void DeleteCurrentProfile()
    {
        SaveLoadManager.DeletePlayer(currentSaveSlotIndex);
    }

    public void CreateNewProfileByInputField()
    {
        SaveLoadManager.CreateNewPlayer(currentSaveSlotIndex, inputField.text);
    }
}
