﻿using UnityEngine;

public class PlanetBehaviour : Planet
{
    protected virtual void Awake()
    {
        var sprites = GetComponentsInChildren<SpriteRenderer>();
        _outerRing = sprites[0];
        _teamGlowRing = sprites[1];
        _powerCounterText = GetComponentInChildren<UnityEngine.UI.Text>();
        _currentPower = initialPower;
    }

    // Use this for initialization
    protected virtual void Start()
    {
        base.Init();
        UpdateTeamColor();
        StageControl.AddPlanetToTeam(team, this);
        _rotationSpeed *= Random.Range(0.75f, 1.25f);
    }

    public override void Update()
    {
        base.Update();
    }

    /// <summary>
    /// Attacks the target.
    /// </summary>
    public override void AttackTarget(Planet attackingPlanet, Planet planetToAttack)
    {
        base.AttackTarget(attackingPlanet, planetToAttack);
    }

    /// <summary>
    /// Deselects a planet
    /// </summary>
    /// <param name="planet"></param>
    private void DeselectPlanet(Planet planet)
    {
        Utility.SelectedPlanet = planet;
        Utility.DeactivateSelectionCircle();
    }

    /// <summary>
    /// Raises the mouse over event.
    /// </summary>
    private void OnMouseOver()
    {
        Utility.MouseOnPlanet = true;
        Utility.planetAtMouse = this;
    }

    /// <summary>
    /// Raises the mouse exit event.
    /// </summary>
    private void OnMouseExit()
    {
        Utility.MouseOnPlanet = false;
    }


}
