﻿using System.Collections.Generic;

public class PlanetConnectionKey
{
    public Planet fromPlanet;
    public Planet toPlanet;

    public PlanetConnectionKey(Planet from, Planet to)
    {
        fromPlanet = from;
        toPlanet = to;
    }

    public class EqualityComparer : IEqualityComparer<PlanetConnectionKey>
    {
        public bool Equals(PlanetConnectionKey x, PlanetConnectionKey y)
        {
            return x.toPlanet == y.toPlanet && x.fromPlanet == y.fromPlanet;
        }

        public int GetHashCode(PlanetConnectionKey obj)
        {
            return (int)(obj.toPlanet.transform.position.x * obj.toPlanet.transform.position.y) ^ (int)(obj.fromPlanet.transform.position.x * obj.fromPlanet.transform.position.y);
        }
    }
}