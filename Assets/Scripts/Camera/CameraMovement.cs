﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

public static class CameraMovement
{
    private static Stack<bool> _shakeHolder = new Stack<bool>();
    private static Stack<bool> _zoomHolder = new Stack<bool>();
    private static Stack<bool> _motionHolder = new Stack<bool>();
    private static Stack<bool> _followHolder = new Stack<bool>();

    public static AnimationCurve MotionCurve;
    public static float Intensity = 0f;

    /// <summary>
    /// Moves the camera from one position to the other with an animation curve
    /// </summary>
    /// <param name="startPosition"></param>
    /// <param name="endPosition"></param>
    /// <returns></returns>
    public static IEnumerator<float> MoveCameraOverTime(Vector3 endPosition, float motionSpeed, AnimationCurve curve)
    {
        _motionHolder.Push(true);

        if (curve == null)
        {
            curve = MotionCurve;
        }

        var progress = 0f;

        while (Camera.main.transform.position != endPosition)
        {
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, endPosition, curve.Evaluate(progress));
            progress += Time.deltaTime * motionSpeed;
            yield return Timing.WaitForOneFrame;
        }

        _motionHolder.Pop();
    }

    /// <summary>
    /// Follows a target transform for a duration in seconds
    /// </summary>
    /// <param name="target"></param>
    /// <param name="duration"></param>
    /// <returns></returns>
    public static IEnumerator<float> FollowTargetForDuration(Transform target, float duration)
    {
        _followHolder.Push(true);

        Camera.main.GetComponent<SmoothCamera2D>().SetTarget(target);

        yield return Timing.WaitForSeconds(duration);

        _followHolder.Pop();

        // Reset the camera when there are no more follower objects
        if (!IsFollowing)
        {
            Camera.main.GetComponent<SmoothCamera2D>().SetTarget(null);
        }
    }

    /// <summary>
    /// Follows a target transform for a duration in seconds and check for end or not
    /// </summary>
    /// <param name="target"></param>
    /// <param name="duration"></param>
    /// <returns></returns>
    public static IEnumerator<float> FollowCinematicTargetForDuration(Transform target, float duration, bool waitForEnd)
    {
        _followHolder.Push(true);

        if (!waitForEnd)
        {
            Utility.GetCinematicController().Progress();
        }

        Camera.main.GetComponent<SmoothCamera2D>().SetTarget(target);

        yield return Timing.WaitForSeconds(duration);

        _followHolder.Pop();

        // Reset the camera when there are no more follower objects
        if (!IsFollowing)
        {
            Camera.main.GetComponent<SmoothCamera2D>().SetTarget(null);
        }

        if (waitForEnd)
        {
            Utility.GetCinematicController().Progress();
        }
    }

    /// <summary>
    /// Moves the camera from one position to the other with an animation curve
    /// </summary>
    /// <param name="startPosition"></param>
    /// <param name="endPosition"></param>
    /// <returns></returns>
    public static IEnumerator<float> ZoomCameraOverTime(float targetZoom, float zoomSpeed, AnimationCurve curve)
    {
        _zoomHolder.Push(true);
        var progress = 0f;

        while (Camera.main.orthographicSize != targetZoom)
        {
            Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, targetZoom, curve.Evaluate(progress));
            progress += Time.deltaTime * zoomSpeed;
            yield return Timing.WaitForOneFrame;
        }

        _zoomHolder.Pop();
    }

    /// <summary>
    /// Shakes the camera with an intensity for a duration of time
    /// </summary>
    /// <returns></returns>
    public static IEnumerator<float> ShakeCamera(float intensity, float duration)
    {
        _shakeHolder.Push(true);

        Intensity = intensity;

        yield return Timing.WaitForSeconds(duration);

        _shakeHolder.Pop();
    }

    /// <summary>
    /// Shakes the camera with an intensity for a duration of time
    /// </summary>
    /// <returns></returns>
    public static IEnumerator<float> ShakeCinematicCamera(float intensity, float duration, bool waitForFinish)
    {
        _shakeHolder.Push(true);

        if (!waitForFinish)
        {
            Utility.GetCinematicController().Progress();
        }

        Intensity = intensity;

        yield return Timing.WaitForSeconds(duration);

        if (waitForFinish)
        {
            Utility.GetCinematicController().Progress();
        }

        _shakeHolder.Pop();
    }

    /// <summary>
    /// Moves the camera from one position to the other with an animation curve
    /// </summary>
    /// <param name="startPosition"></param>
    /// <param name="endPosition"></param>
    /// <returns></returns>
    public static IEnumerator<float> MoveCinematicCameraOverTime(Vector3 endPosition, float motionSpeed, AnimationCurve curve, bool waitForFinish)
    {
        _motionHolder.Push(true);

        if (!waitForFinish)
        {
            Utility.GetCinematicController().Progress();
        }

        if (curve == null)
        {
            curve = MotionCurve;
        }

        var progress = 0f;

        while (Camera.main.transform.position != endPosition)
        {
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, endPosition, curve.Evaluate(progress));
            progress += Time.deltaTime * motionSpeed;
            yield return Timing.WaitForOneFrame;
        }

        if (waitForFinish)
        {
            Utility.GetCinematicController().Progress();
        }

        _motionHolder.Pop();
    }

    /// <summary>
    /// Moves the camera from one position to the other with an animation curve
    /// </summary>
    /// <param name="startPosition"></param>
    /// <param name="endPosition"></param>
    /// <returns></returns>
    public static IEnumerator<float> ZoomCinematicCameraOverTime(float targetZoom, float zoomSpeed, AnimationCurve curve, bool waitForFinish)
    {
        _zoomHolder.Push(true);
        var progress = 0f;

        if (!waitForFinish)
        {
            Utility.GetCinematicController().Progress();
        }

        while (Camera.main.orthographicSize != targetZoom)
        {
            Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, targetZoom, curve.Evaluate(progress));
            progress += Time.deltaTime * zoomSpeed;
            yield return Timing.WaitForOneFrame;
        }

        if (waitForFinish)
        {
            Utility.GetCinematicController().Progress();
        }

        _zoomHolder.Pop();
    }

    public static bool IsShaking
    {
        get
        {
            return _shakeHolder.Count > 0;
        }
    }

    public static bool IsMoving
    {
        get
        {
            return _motionHolder.Count > 0;
        }
    }

    public static bool IsZooming
    {
        get
        {
            return _zoomHolder.Count > 0;
        }
    }

    public static bool IsFollowing
    {
        get
        {
            return _followHolder.Count > 0;
        }
    }
}