﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileSiloScript : MonoBehaviour
{
    private float _maxSpeed = 5f;
    private float _currentSpeed = 0f;
    private float _destinationRadius = 1f;
    private float _damagePercentage = 15f;
    private Planet _target;
    private Team _team;

    [SerializeField]
    private SpriteRenderer glow;

    public void SetData(Planet target, Team missileTeam, float damagePercentage)
    {
        _team = missileTeam;
        _target = target;
        _damagePercentage = damagePercentage;

        glow.color = Utility.GetTeamColor(_team);
    }

    private void Update()
    {
        if (GameController.GameState == GameStates.Playing)
        {
            // Has to have a target to carry out actions
            if (_target)
            {
                // Rotate towards target
                transform.eulerAngles = new Vector3(0f, 0f, Mathf.LerpAngle(transform.eulerAngles.z, Mathf.Atan2(_target.transform.position.y - transform.position.y, _target.transform.position.x - transform.position.x) * Mathf.Rad2Deg - 90f, Time.fixedDeltaTime * 15f));

                // When the missile's team is the same as the target's team, explode | TODO change target when possible
                if (_target.team == _team)
                {
                    Destroy(gameObject);
                }

                // Distance to target check, when close enough explode
                if ((_target.transform.position - transform.position).magnitude < _destinationRadius)
                {
                    // When the target has less ships than the explosion will deal as damage, destroy all ships and change owner to neutral
                    if (_target.Power < _target.MaxPower * (_damagePercentage / 100f))
                    {
                        _target.RemovePower(_target.Power); // TODO can generate new power when the coroutine runs
                        _target.team = Team.Neutral;
                        _target.UpdateTeamColor();
                    }
                    else
                    {
                        _target.RemovePower(_target.MaxPower * (_damagePercentage / 100f));
                    }

                    Destroy(gameObject);
                }

                // Accelerate
                _currentSpeed += (_currentSpeed < _maxSpeed) ? _maxSpeed / 100f : 0f;

                // Move towards the target
                transform.position += (_target.transform.position - transform.position).normalized * _currentSpeed * Time.deltaTime;
            }
        }
    }
}
