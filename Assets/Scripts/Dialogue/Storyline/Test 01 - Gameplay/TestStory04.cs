﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestStory04 : ActionControllerBase
{
    /// <summary>
    /// Campaign Mission 01: The Start of a Rebellion
    /// </summary>
    protected override void InitCinematicActions()
    {
        _cinematicActions.Add(new CinematicActionScreenShake(1f, 0f, true));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "Stage four, great!"));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "In this scenario you will fight an opponent who has a <color='cyan'>Guardian</color> planet. \nTake over the nearest <color='green'>Regenerator</color> planet. These planets generate ships more quickly and produce the Sprinter ship which moves faster!"));

        _cinematicActions.Add(new CinematicActionDialogue(
            _dialogueManager,
            Character.TG04,
            DialogueDirection.Left,
            "The enemy has a couple of <color='cyan'>Guardian</color> planets, strike quickly before he attacks."));
    }
}
