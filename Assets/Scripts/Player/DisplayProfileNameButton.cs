﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayProfileNameButton : MonoBehaviour
{

    private Text textToChange;
    private Color startingColor;
    private string emptySaveSlotText = "Empty Save Slot";
    public int index = 0;

    // Use this for initialization
    void Awake()
    {
        textToChange = GetComponentInChildren<Text>();
        startingColor = textToChange.color;
    }

    // Update is called once per frame
    void Update()
    {
        if (textToChange.text == emptySaveSlotText && Time.frameCount % 2 == 0)
        {
            textToChange.color = startingColor;
            textToChange.text = SaveLoadManager.GetProfileName(index);
        }
        else if (textToChange.text != emptySaveSlotText && Time.frameCount % 2 == 0)
        {
            textToChange.color = Color.yellow;
            textToChange.text = SaveLoadManager.GetProfileName(index);
        }
    }
}
