﻿using UnityEngine;

public class CampaignMapButton : MonoBehaviour
{

    public GameObject infoBox;
    private bool show = false;

    private void Start()
    {
        show = infoBox.activeSelf;
    }

    public void ShowInfoBox()
    {
        show = !show;
        infoBox.SetActive(show);
    }
}
