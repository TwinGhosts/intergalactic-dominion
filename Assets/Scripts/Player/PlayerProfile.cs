﻿using UnityEngine;

public class PlayerProfile : MonoBehaviour
{

    public PlayerData data;

    public bool firstTime = true;
    public int onlineRating;
    public int winRating;
    public Color teamColor = Color.green;
    public new string name = "Generic Name";

    public void Start()
    {
        if (firstTime)
        {
            firstTime = false;
            onlineRating = 1000;
            winRating = 0;
        }
    }


}
