﻿using System.Collections;
using UnityEngine;

public class PlanetGuardian : PlanetBehaviour
{

    [Header("Shield Properties")]
    [SerializeField]
    private GameObject _shieldObject;

    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();
        SetStats();
    }

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
    }

    /// <summary>
    /// Sets all of the basic properties and values of the planet
    /// </summary>
    protected override void SetStats()
    {
        base.SetStats();
        switch (size)
        {
            case PlanetSize.Tiny:

                break;
            case PlanetSize.Small:

                break;
            default:
            case PlanetSize.Normal:

                break;
            case PlanetSize.Large:

                break;
            case PlanetSize.Huge:

                break;
            case PlanetSize.Gigantic:

                break;
        }

        GetComponent<CircleCollider2D>().radius = 0.4f;
        _chargeRate = CHARGE_RATE * 1.4f;
        _maxPower = MAX_POWER;
    }
}
