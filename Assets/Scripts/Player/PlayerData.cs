﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class SaveLoadManager
{
    private static string path = "/player_profile";
    private static string extension = ".sav";

    public static void DeletePlayer(int index)
    {
        var totalPath = Application.persistentDataPath + path + index + extension;
        File.Delete(totalPath);
    }

    /// <summary>
    /// Saves the current chosen playerData
    /// </summary>
    /// <param name="index"></param>
	public static void SavePlayer(int index, PlayerData data)
    {
        var totalPath = Application.persistentDataPath + path + index + extension;

        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(totalPath, FileMode.Create);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    /// <summary>
    /// Creates and saves a new player
    /// </summary>
    /// <param name="index"></param>
    public static void CreateNewPlayer(int index, string name)
    {
        PlayerData data = new PlayerData(name);
        SavePlayer(index, data);
    }

    public static string GetProfileName(int index)
    {
        var totalPath = Application.persistentDataPath + path + index + extension;
        if (File.Exists(totalPath))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(totalPath, FileMode.Open);

            PlayerData data = formatter.Deserialize(stream) as PlayerData;
            stream.Close();
            return data.name;
        }
        else
        {
            return "Empty Save Slot";
        }
    }

    /// <summary>
    /// Loads the playerData from the index
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
	public static bool LoadPlayer(int index)
    {
        var totalPath = Application.persistentDataPath + path + index + extension;

        if (File.Exists(totalPath))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(totalPath, FileMode.Open);

            CurrentPlayer.data = formatter.Deserialize(stream) as PlayerData;
            stream.Close();
            return true;
        }
        else
        {
            return false;
        }
    }
}

[Serializable]
public class PlayerData
{
    public float[] colors = new float[3];
    public string name;
    public int onlineRating;

    // TODO: Save Online Play Data
    // TODO: Save Campaign Data

    public PlayerData(string name)
    {
        this.name = name;
        colors[0] = 255f;
        colors[1] = 0f;
        colors[2] = 0f;
        onlineRating = 1000;
    }
}

public static class CurrentPlayer
{
    public static PlayerData data;
}