﻿using System.Collections;
using UnityEngine;
using MEC;
using System.Collections.Generic;

public class PlanetConnectionInfo
{
    public Planet startingPlanet;
    public Planet targetPlanet;
    public Vector3 currentPosition;
    public float connectionSpeed;
    public bool isConnected;
    public bool isTryingToConnect;
    public float progress;
    public bool isAttacking;
    public GameObject lineObject;
    public bool delete;

    public PlanetConnectionInfo(Planet startingPlanet, Planet targetPlanet, float connectionSpeed)
    {
        this.startingPlanet = startingPlanet;
        this.targetPlanet = targetPlanet;
        this.connectionSpeed = connectionSpeed;
        progress = 0f;
        currentPosition = this.startingPlanet.transform.position;
        isAttacking = false;
    }

    public void UpdateConnectionProgress()
    {
        // Create the line
        if (!lineObject)
        {
            lineObject = Utility.CreateLaser(startingPlanet, targetPlanet);
            lineObject.GetComponent<LineBehaviour>().Connect(startingPlanet, targetPlanet, connectionSpeed);
        }

        // Start attacking when the planet isn't attacking and connected
        if (lineObject.GetComponent<LineBehaviour>().IsConnected && !isAttacking)
        {
            Timing.RunCoroutine(Attack());
        }
    }

    public IEnumerator<float> Attack()
    {
        isAttacking = true;

        yield return Timing.WaitForSeconds(startingPlanet.AttackRate);

        if (targetPlanet.Power > 0f)
        {
            if (!StageControl.TeamIsAllyOf(startingPlanet.team, targetPlanet.team))
            {
                targetPlanet.RemovePower(startingPlanet.AttackPower);
                startingPlanet.RemovePower(startingPlanet.AttackPower);
            }
            else
            {
                targetPlanet.AddPower(startingPlanet.AttackPower);
                startingPlanet.RemovePower(startingPlanet.AttackPower);
            }
        }
        else
        {
            StageControl.SwitchPlanetOwner(startingPlanet, targetPlanet);
        }

        isAttacking = false;
    }

    public void BreakConnection()
    {
        lineObject.GetComponent<LineBehaviour>().Disconnect();
        Utility.RemoveFromDict(startingPlanet.ConnectionInfo, new PlanetConnectionKey(startingPlanet, targetPlanet));
    }
}